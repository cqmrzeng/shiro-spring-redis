package com.hh.shiro.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hh.shiro.JedisManager;
import com.hh.shiro.SerializeUtil;

public class JedisShiroCache<K, V> implements Cache<K, V> {
	private static final Logger LOGGER = LoggerFactory.getLogger(JedisShiroCache.class);

    private static final String REDIS_SHIRO_CACHE = "hh-shiro-redis-cache:";
    private static final int DB_INDEX = 1;

    private JedisManager jedisManager;

    private String name;

    public JedisShiroCache(String name, JedisManager jedisManager) {
        this.name = name;
        this.jedisManager = jedisManager;
    }

    @SuppressWarnings("unchecked")
    @Override
    public V get(K key) throws CacheException {
    	LOGGER.trace("get object from redis by key:[{}]", key);
    	try {
    		if (key == null) {
				return null;
			} else {
		        byte[] byteKey = SerializeUtil.serialize(buildCacheKey(key));
		        byte[] byteValue = jedisManager.getValueByKey(DB_INDEX, byteKey);
	            return (V) SerializeUtil.deserialize(byteValue);
			}
        } catch (Throwable t) {
			throw new CacheException(t);
		}
    }

    @Override
	public V put(K key, V value) throws CacheException {
    	LOGGER.trace("根据key从存储 key [" + key + "]");
		try {
			V previos = get(key);
			jedisManager.saveValueByKey(DB_INDEX,
					SerializeUtil.serialize(buildCacheKey(key)),
					SerializeUtil.serialize(value), -1);
			return previos;
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

    @Override
    public V remove(K key) throws CacheException {
    	LOGGER.trace("从redis中删除 key [" + key + "]");
        V previos = get(key);
        try {
            jedisManager.deleteByKey(DB_INDEX, SerializeUtil.serialize(buildCacheKey(key)));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("remove cache error");
        }
        return previos;
    }

    @Override
    public void clear() throws CacheException {
    	LOGGER.trace("从redis中删除所有元素");
    	try {
    		jedisManager.flushDB();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
    }

    @Override
    public int size() {
        try {
			Long longSize = new Long(jedisManager.dbSize());
			return longSize.intValue();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
    }

	@Override
	@SuppressWarnings("unchecked")
    public Set<K> keys() {
    	try {
			Set<byte[]> keys = jedisManager.keys(String.format("%s%s%s", REDIS_SHIRO_CACHE, getName(), "*"));
			if (CollectionUtils.isEmpty(keys)) {
				return Collections.emptySet();
			} else {
				Set<K> newKeys = new HashSet<K>();
				for (byte[] key : keys) {
					newKeys.add((K) key);
				}
				return newKeys;
			}
		} catch (Throwable t) {
			throw new CacheException(t);
		}
    }

    @Override
    public Collection<V> values() {
		try {
			Set<byte[]> keys = jedisManager.keys(String.format("%s%s%s", REDIS_SHIRO_CACHE, getName(), "*"));
			if (!CollectionUtils.isEmpty(keys)) {
				List<V> values = new ArrayList<V>(keys.size());
				for (byte[] key : keys) {
					@SuppressWarnings("unchecked")
					V value = get((K) key);
					if (value != null) {
						values.add(value);
					}
				}
				return Collections.unmodifiableList(values);
			} else {
				return Collections.emptyList();
			}
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

    private String buildCacheKey(Object key) {
        return String.format("%s%s:%s", REDIS_SHIRO_CACHE, getName(), key);
    }
    
    /**
     * 自定义relm中的授权/认证的类名加上授权/认证英文名字
     */
    public String getName() {
        if (StringUtils.isBlank(name)) {
        	return StringUtils.EMPTY;
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
