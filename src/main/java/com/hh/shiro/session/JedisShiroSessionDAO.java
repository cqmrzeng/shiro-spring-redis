package com.hh.shiro.session;

import java.io.Serializable;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JedisShiroSessionDAO extends CachingSessionDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(JedisShiroSessionDAO.class);

    private ShiroSessionRepository shiroSessionRepository;

	@Override
	protected void doUpdate(Session session) {
		LOGGER.trace("update session:{}", session);
        getShiroSessionRepository().saveSession(session);
	}

	@Override
	protected void doDelete(Session session) {
		if (session == null) {
            return;
        }
        Serializable id = session.getId();
        if (id != null) {
            LOGGER.trace("delete session:{}", session);
            getShiroSessionRepository().deleteSession(id);
        }
	}

    @Override
    protected Serializable doCreate(Session session) {
        LOGGER.trace("do create session:{}", session);
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        getShiroSessionRepository().saveSession(session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        LOGGER.trace("do read session:{}", sessionId);
        return getShiroSessionRepository().getSession(sessionId);
    }

    public ShiroSessionRepository getShiroSessionRepository() {
        return shiroSessionRepository;
    }

    public void setShiroSessionRepository(
            ShiroSessionRepository shiroSessionRepository) {
        this.shiroSessionRepository = shiroSessionRepository;
    }

}
